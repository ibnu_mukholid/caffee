window.googletag = window.googletag || {};
window.googletag.cmd = window.googletag.cmd || [];
(function (W, D, N) {
W[N]=W[N]||{};W[N].cmd=W[N].cmd||[];W[N].cmd2=W[N].cmd2||[];
function getDbg(){var dbg=0,m;try{m = W.location.href.match(/pbjs_debug=(\S*)/) || (D.cookie+';').match(/pbjs_debug=(\S*)\;/);dbg=m && m[1] && 'true'===(m[1].split('&')[0]||'')}catch(e){}D.cookie='pbjs_debug='+dbg+'; path=/; secure';return dbg}
W.G_options=W.G_options||{};
W.G_options.debug=getDbg();
var G_debug=G_options.debug;
function loadScript(url){var o='script',s=document,a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=url;m.parentNode.insertBefore(a,m);};



(function (W, D, N) {
"use strict";
try {
W[N] = W[N] || {};
var inparaGetPos_impl=function(cfg) {
var
dataFound = 0,
INP = W[N].INP,
cfg_def = cfg.def,
opt = {
paraTxtMinLen: cfg_def.paraTxtMinLen,
minAdWidth: cfg_def.minAdWidth,
mxTopDistPerc: cfg_def.mxTopDistPerc,
selChild: cfg.selChild || '',
virtParaMinHeight: cfg_def.virtParaMinHeight * screen.height,
ignorePar: cfg_def.ignorePar
};
var BODY = D.body;
cfg.arCurSel.forEach(function (strSelector) {
if (dataFound) {
return
}
var rootNode = BODY.querySelector(strSelector);
if (rootNode) {
var
arItems = INP.getvalidTextnodes(rootNode, opt),
data = INP.chooseTextNode(arItems, opt);
if (data) {
if (G_debug) {
cfg.debug_overlay_all = function (xtra) {
document.querySelectorAll('span.__aff_inpara_overlay,span.__aff_inpara_overlay2,span.__aff_inpara_overlay3,span.__aff_inpara_overlay4,span.__aff_inpara_overlay5').forEach(function (xDbgEle) {
var p = xDbgEle.parentElement;
p.removeChild(xDbgEle);
});
xtra = xtra || {};
INP.debug_overlay(data.arItems, '__aff_inpara_overlay', 'arItems#1', {
"arbgColor": ['red', 'green', 'blue', 'pink', 'yellow', 'black']
}, xtra);
INP.debug_overlay(data.combineParaSamePar, '__aff_inpara_overlay2', 'combineParaSamePar#2', {}, xtra);
INP.debug_overlay(data.combineParaRectOverlap, '__aff_inpara_overlay3', 'combineParaRectOverlap#3', {}, xtra);
INP.debug_overlay(data.combineParaAsColum, '__aff_inpara_overlay4', 'combineParaAsColum#4', {}, xtra);
INP.debug_overlay(data.combineParaAsVirtualSplit, '__aff_inpara_overlay5', 'combineParaAsVirtualSplit#5', {
fillColor: 'yellow'
}, xtra);
};
cfg.debug_overlay_all({
adRect: {
b: 0,
h: 0
}
});
INP.debug_overlay_all = cfg.debug_overlay_all;
}
var ret = [];
INP.forEach(data.combineParaAsVirtualSplit, function (data, idx) {
var
anchorEleFirst = data.idx[0],
anchorEleLast = data.idx[data.idx.length - 1];
if (!opt.selChild) {
if (rootNode !== anchorEleFirst.parentNode) {
anchorEleFirst = anchorEleFirst.parentNode;
}
if (rootNode !== anchorEleLast.parentNode) {
anchorEleLast = anchorEleLast.parentNode;
}
}
ret.push({
eleFirst: anchorEleFirst,
eleLast: anchorEleLast,
ele: anchorEleLast,
rect: data.rect
});
});
if (ret.length) {
cfg.processAdspotData(ret);
dataFound = 1;
}
}
}
});
return dataFound;
}
W[N].inparaGetPos = function (cfg) {
inparaGetPos_impl(cfg);
};
} catch (e) {
}
})(window, document, '__afflib');

if(W[N]['affmp2_detik.com']){return}W[N]['affmp2_detik.com']=1;
loadScript('https://cdn4-hbs.affinitymatrix.com/hvrlib/detik.com/1628675995/v2.js');
W[N].cmd.push(function(){
if(!W[N].chkDomain('detik.com')){return}
var cfg = {"aff":{"def":{"maxCall":1,"minVisblePerc":50,"delaySec":30,"kw":{"domain":"detik.com"},"gads":{"enabled":true,"rmslot":0,"cmpsz":{"w_max_perc":2,"h_max_perc":2,"szMobileOnly":["320x50"]},"vspace":"middle","allowWoSlot":1},"moat":{"enabled":true}},"aus":[{"au":"/42115163,4905536/IP_detik.com_ALL_Multisize_RON_Both_New_HVR","sz":["160x600","300x250","728x90"],"def":1},{"au":"/21930596546,4905536/IP_detik.com_ALL_Multisize_RON_Both_New_HVR_MC","sz":["160x600","300x250","728x90"],"def":1}],"pbjs":{"enabled":true,"nm":"affpbjs","hbsite":"hvr_man_detik.com"}},"pub":{"def":{"maxCall":1,"delaySec":30,"minVisblePerc":50,"reprf":4,"kw":{"domain":"detik.com"},"excludePatrn":{"enable":1,"patrn":"NO_REFRESH"},"section":{"enable":0,"whitelist":[],"blacklist":[]},"ignireImpForPubAu":0,"dfpids":{"enable":0,"incIds":[],"excIds":[]}},"rule":[{"tp":"exc","au":["*"],"sz":["1x1"],"lbl":"Ignr 1x1"},{"tp":"inc","au":["*"],"sz":["160x600","300x250","728x90"],"lbl":"All Au"},{"tp":"exc","au":["/4905536/detik_mobile/news/billboard","/4905536/detik_mobile/news/hiddenquiz","/4905536/detik_mobile/news/inbetween","/4905536/detik_mobile/news/parallax_detail","/4905536/detik_mobile/news/mega_billboard","/4905536/detik_mobile/news/newstag","/4905536/detik_mobile/finance/billboard","/4905536/detik_mobile/finance/hiddenquiz","/4905536/detik_mobile/finance/inbetween","/4905536/detik_mobile/finance/parallax_detail","/4905536/detik_mobile/finance/mega_billboard","/4905536/detik_mobile/finance/newstag","/4905536/detik_mobile/food/billboard","/4905536/detik_mobile/food/hiddenquiz","/4905536/detik_mobile/food/inbetween","/4905536/detik_mobile/food/parallax_detail","/4905536/detik_mobile/food/mega_billboard","/4905536/detik_mobile/food/newstag","/4905536/detik_mobile/health/billboard","/4905536/detik_mobile/health/hiddenquiz","/4905536/detik_mobile/health/inbetween","/4905536/detik_mobile/health/parallax_detail","/4905536/detik_mobile/health/mega_billboard","/4905536/detik_mobile/health/newstag","/4905536/detik_mobile/hot/billboard","/4905536/detik_mobile/hot/hiddenquiz","/4905536/detik_mobile/hot/inbetween","/4905536/detik_mobile/hot/parallax_detail","/4905536/detik_mobile/hot/mega_billboard","/4905536/detik_mobile/hot/newstag","/4905536/detik_mobile/inet/billboard","/4905536/detik_mobile/inet/hiddenquiz","/4905536/detik_mobile/inet/inbetween","/4905536/detik_mobile/inet/parallax_detail","/4905536/detik_mobile/inet/mega_billboard","/4905536/detik_mobile/inet/newstag","/4905536/detik_mobile/oto/billboard","/4905536/detik_mobile/oto/hiddenquiz","/4905536/detik_mobile/oto/inbetween","/4905536/detik_mobile/oto/parallax_detail","/4905536/detik_mobile/oto/mega_billboard","/4905536/detik_mobile/oto/newstag","/4905536/detik_mobile/sepakbola/billboard","/4905536/detik_mobile/sepakbola/hiddenquiz","/4905536/detik_mobile/sepakbola/inbetween","/4905536/detik_mobile/sepakbola/parallax_detail","/4905536/detik_mobile/sepakbola/mega_billboard","/4905536/detik_mobile/sepakbola/newstag","/4905536/detik_mobile/sport/billboard","/4905536/detik_mobile/sport/hiddenquiz","/4905536/detik_mobile/sport/inbetween","/4905536/detik_mobile/sport/parallax_detail","/4905536/detik_mobile/sport/mega_billboard","/4905536/detik_mobile/sport/newstag","/4905536/detik_mobile/travel/billboard","/4905536/detik_mobile/travel/hiddenquiz","/4905536/detik_mobile/travel/inbetween","/4905536/detik_mobile/travel/parallax_detail","/4905536/detik_mobile/travel/mega_billboard","/4905536/detik_mobile/travel/newstag","/4905536/detik_mobile/wolipop/billboard","/4905536/detik_mobile/wolipop/hiddenquiz","/4905536/detik_mobile/wolipop/inbetween","/4905536/detik_mobile/wolipop/parallax_detail","/4905536/detik_mobile/wolipop/mega_billboard","/4905536/detik_mobile/wolipop/newstag"],"sz":["*"],"lbl":"Black-AU"}],"adspots":[]},"geo":{"enable":false,"srvdrct":true,"cc":["*"]}};
if('function' === typeof W[N].hvrCustCfg ){
    cfg = W[N].hvrCustCfg( cfg );
}
if(cfg.aff.pbjs && cfg.aff.pbjs.enabled){
    var hbsite = cfg.aff.pbjs.hbsite || ('hvr_' + cfg.aff.def.kw.domain),d=new Date(),
    hbInvKey='affhb2_affpbjs_' + cfg.aff.pbjs.hbsite.replace('hvr_man_','hvr_');
    if(!W[N][hbInvKey]){        
    W[N].U.loadScript('https://hbs.ph.affinity.com/v5/' + hbsite + '/affhb.data.js.php?t=' + d.getDate() + d.getMonth() + d.getHours() );
    }
}
if( cfg.aff.def.dfpenblsrv){W[N].U.loadScript("https://securepubads.g.doubleclick.net/tag/js/gpt.js");}

    googletag.cmd.push(function() {
        W[N].AffRefresh(cfg)
    });
});
})(window, document, '__afflib');